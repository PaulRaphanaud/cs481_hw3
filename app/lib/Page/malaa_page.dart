import 'package:flutter/material.dart';
import '../Section/text_section.dart';

class MalaaPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Image.asset(
          "assets/malaa.jpeg",
          fit: BoxFit.cover,
          height: double.infinity,
          width: double.infinity,
          alignment: Alignment.center,
        ),
        TextSection(
            text: 'Malaa is a French electronic music DJ and producer'
                'who is signed to Tchami\'s label Confession.'
                'He broke onto the electronic music scene'
                'through his single \"Notorious\",'
                'which was the second release on Confession.'
                'His identity is unknown'
                'as he appears in public as a balaclava-wearing man.'),
      ],
    );
  }
}
