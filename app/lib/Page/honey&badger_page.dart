import 'package:flutter/material.dart';
import '../Section/text_section.dart';

class HoneyBadgerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Image.asset(
          "assets/h&b.jpg",
          fit: BoxFit.cover,
          height: double.infinity,
          width: double.infinity,
          alignment: Alignment.center,
        ),
        TextSection(
            text: 'Honey & Badger is a Belgian DJ & Producer '
                'who keeps making waves since he got noticed by Tchami in 2018. '
                'Fidget, bass and groovy vibes combined with his tracks’ names '
                'referring to Belgium’s dopest goods '
                'demonstrate his devotion to both house music and his country.'),
      ],
    );
  }
}
