import 'package:app/Page/honey&badger_page.dart';
import 'package:app/Widget/labelbutton_widget.dart';
import './malaa_page.dart';
import './tchami_page.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Widget> _pages = [MalaaPage(), TchamiPage(), HoneyBadgerPage()];
  int _pageIndex = 0;

  changeDj() {
    setState(() {
      if (_pageIndex == _pages.length - 1) {
        _pageIndex = 0;
      } else {
        _pageIndex += 1;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          centerTitle: true,
          backgroundColor: Colors.grey[900],
          elevation: 0.0,
        ),
        body: Stack(
          children: [
            _pages[_pageIndex],
            Positioned(
              right: 20,
              top: 20,
              child: LabelIconButton(
                color: Colors.grey[200],
                icon: Icons.refresh,
                label: "Change",
                onPress: changeDj,
              ),
            )
          ],
        ));
  }
}
