import 'package:flutter/material.dart';
import '../Section/text_section.dart';

class TchamiPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Image.asset(
          "assets/tchami.jpeg",
          fit: BoxFit.cover,
          height: double.infinity,
          width: double.infinity,
          alignment: Alignment.center,
        ),
        TextSection(
            text: 'Tchami is a French record producer and DJ from Paris.'
                'A founding member of the Pardon My French collective, '
                'he is best known for his solo work'
                ' and regarded as a pioneer of the future house genre '
                'alongside Dutch DJs Oliver Heldens and Don Diablo.'
                'Tchami often performs with the persona of a priest and a church theme.'),
      ],
    );
  }
}
