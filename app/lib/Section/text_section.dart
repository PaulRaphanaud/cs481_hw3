import 'package:flutter/material.dart';

class TextSection extends StatelessWidget {
  TextSection({Key key, this.text}) : super(key: key);

  @required
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black45,
      padding: const EdgeInsets.all(32),
      child: Text(
        text,
        style: TextStyle(color: Colors.white),
        softWrap: true,
      ),
    );
  }
}
