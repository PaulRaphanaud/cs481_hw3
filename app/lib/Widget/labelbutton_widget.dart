import 'package:flutter/material.dart';

class LabelIconButton extends StatelessWidget {
  LabelIconButton({Key key, this.icon, this.color, this.label, this.onPress})
      : super(key: key);

  @required
  final Color color;
  @required
  final IconData icon;
  @required
  final String label;
  final Function onPress;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            icon,
            color: color,
          ),
          Container(
            margin: const EdgeInsets.only(top: 8),
            child: Text(
              label,
              style: TextStyle(
                  fontSize: 12, fontWeight: FontWeight.w400, color: color),
            ),
          )
        ],
      ),
    );
  }
}
